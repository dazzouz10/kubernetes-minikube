# kubernetes-minikube

## minikube start

 Ouvrir Docker 

 Se loguer sur Docker hub : 

* `docker login`      ou
* `docker login http://hub.docker.com`    ou
* `docker login -u username -p password`

 Se mettre sur le terminal de lordinateur et lancer les commandes suivante : 
 `minikube start`

 `minikube dashboard`

### Instalation 

Utiliser git: `git clone https://gitlab.com/dazzouz10/kubernetes-minikube.git`  

Se mettre sur le dossier MyService ou se trouve le DockerFile : `cd kubernetes-minikube/MyService`

## Docker 

Pour build lancer la commande suivante sur le terminal Visual Studio Code : `.\gradlew build`

Construire l'image docker : `docker build -t myservice .`

Check l'image : `docker images`

Lancer le container: `docker run -p 4000:8080 -t myservice`

8080 est le port du web service,

4000 est le port qui donne acces au container. 

Possible de tester avec cette adresse : http://localhost:4000 

hello

Ctrl-C pour arreter le web service.

Check l'Id du container: `docker ps`

Arrêter le container: `docker stop containerID`

Tag l'image docker : `docker tag imageID damia10/minikube:version_0`

Push the image to the docker hub: `docker push damia10/minikube:version_0`

## Minikube 

Vérifier que minikube est bien lancer sur le terminal de l'ordinateur : `minikube start`

Revenir sur le terminal VS Code et lancer les commandes suivantes : 

* `docker container list`
*  `kubectl get nodes`
